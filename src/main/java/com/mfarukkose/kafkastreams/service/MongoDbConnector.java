package com.mfarukkose.kafkastreams.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoDbConnector {

    MongoCollection<Document> collection;

    public MongoDbConnector(String connectionUri, String databaseName, String collectionName) {
        try {
            MongoClient mongoClient = MongoClients.create(connectionUri);
            MongoDatabase database = mongoClient.getDatabase(databaseName);
            collection = database.getCollection(collectionName);
        } catch (Exception e) {
            throw new RuntimeException("Error on MongoDb connection", e);
        }
    }

    public MongoCollection<Document> getCollection() {
        return collection;
    }

}
