package com.mfarukkose.kafkastreams.service;

import com.mfarukkose.kafkastreams.models.Room;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class RoomService {

    private String databaseName = "realTimeMessaging";
    private String connectionUri = "mongodb+srv://faruk:327855@cluster0.cv7la.mongodb.net/" +
            databaseName
            + "?retryWrites=true&w=majority";
    private String collectionName = "room";
    MongoCollection<Document> collection;

    public RoomService() {
        MongoDbConnector mongoDbConnector = new MongoDbConnector(connectionUri, databaseName, collectionName);
        //collection = mongoDbConnector.getCollection();
    }

    public boolean createRoom(Room room) {
        try {
            Document document = new Document("_id", room.getId())
                    .append("roomName", room.getRoomName())
                    .append("userList", room.getUserList());
            collection.insertOne(document);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}