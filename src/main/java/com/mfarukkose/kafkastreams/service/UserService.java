package com.mfarukkose.kafkastreams.service;

import com.mfarukkose.kafkastreams.models.User;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class UserService {

    private String databaseName = "realTimeMessaging";
    private String connectionUri = "mongodb+srv://faruk:327855@cluster0.cv7la.mongodb.net/" +
            databaseName
            + "?retryWrites=true&w=majority";
    private String collectionName = "user";
    MongoCollection<Document> collection;

    public UserService() {
        MongoDbConnector mongoDbConnector = new MongoDbConnector(connectionUri, databaseName, collectionName);
        //collection = mongoDbConnector.getCollection();
    }

    public boolean createUser(User user) {
        try {
            Document document = new Document("_id", user.getId())
                    .append("name", user.getName())
                    .append("phoneNumber", user.getPhoneNumber())
                    .append("sessionId",user.getSessionId());
            collection.insertOne(document);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
