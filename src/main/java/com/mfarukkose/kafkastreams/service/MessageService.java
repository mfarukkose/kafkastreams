package com.mfarukkose.kafkastreams.service;

import com.mfarukkose.kafkastreams.models.Message;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MessageService {

    private String databaseName = "realTimeMessaging";
    private String connectionUri = "mongodb+srv://faruk:327855@cluster0.cv7la.mongodb.net/" +
            databaseName
            + "?retryWrites=true&w=majority";
    private String collectionName = "message";
    MongoCollection<Message> collection;

    public MessageService() {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        ConnectionString connectionString = new ConnectionString(connectionUri);
        MongoClientSettings settings = MongoClientSettings.builder()
                .codecRegistry(pojoCodecRegistry)
                .applyConnectionString(connectionString)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoDatabase database = mongoClient.getDatabase(databaseName);
        collection = database.getCollection("message", Message.class);
    }

    public boolean sendMessage(Message message) {
        try {
            collection.insertOne(message);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
