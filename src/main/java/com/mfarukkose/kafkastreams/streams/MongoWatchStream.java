package com.mfarukkose.kafkastreams.streams;

import com.google.gson.Gson;
import com.mfarukkose.kafkastreams.models.Message;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoWatchStream {

    private String databaseName = "realTimeMessaging";
    private String connectionUri = "mongodb+srv://faruk:327855@cluster0.cv7la.mongodb.net/" +
            databaseName
            + "?retryWrites=true&w=majority";
    private String collectionName = "message";
    MongoCollection<Message> collection;

    public MongoWatchStream(){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        ConnectionString connectionString = new ConnectionString(connectionUri);
        MongoClientSettings settings = MongoClientSettings.builder()
                .codecRegistry(pojoCodecRegistry)
                .applyConnectionString(connectionString)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoDatabase database = mongoClient.getDatabase(databaseName);
        MongoCollection<Message> collection = database.getCollection("message", Message.class);


        List<Bson> pipeline = singletonList(Aggregates.match(
                Filters.in("operationType", Arrays.asList("insert"))));
        MongoCursor<ChangeStreamDocument<Message>> watchCursor =
                collection.watch().iterator();
        System.out.println(String.valueOf(watchCursor));
        System.out.println("== open cursor ==");

        Runnable task = () -> {
            System.out.println("Waiting for events");
            while (watchCursor.hasNext()) {
                ChangeStreamDocument changeStreamDocument = watchCursor.next();
                Gson gson = new Gson();
                Message message = (Message) changeStreamDocument.getFullDocument();
                try {
                    sendPost(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        new Thread(task).start();
    }

    private void sendPost(Message message) throws Exception {

        HttpPost post = new HttpPost("http://localhost:8080/sendMessageToSocket");

        Gson gson = new Gson();
        StringEntity messageEntity = new StringEntity(gson.toJson(message, Message.class));
        post.addHeader("content-type", "application/json");
        post.addHeader("Accept","application/json");
        post.setEntity(messageEntity);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }

}
