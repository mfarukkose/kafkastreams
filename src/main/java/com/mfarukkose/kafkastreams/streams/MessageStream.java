package com.mfarukkose.kafkastreams.streams;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfarukkose.kafkastreams.models.Message;
import com.mfarukkose.kafkastreams.service.MessageService;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;

import java.io.IOException;
import java.util.Properties;

public class MessageStream{

    private String inputTopic = "usertopic";
    private String bootstrapServers = "192.168.1.46:29092";

    private Properties streamsConfiguration;
    private StreamsBuilder builder;
    private KafkaStreams streams;
    private MessageService messageService;

    public MessageStream() throws Exception {
        KafkaStream kafkaStream = new KafkaStream();
        streamsConfiguration = kafkaStream.getStreamsConfiguration(bootstrapServers);
        builder = new StreamsBuilder();
        listener(builder);
        streams = new KafkaStreams(builder.build(), streamsConfiguration);
        streams.cleanUp();
        streams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        messageService = new MessageService();
    }

    private void listener(final StreamsBuilder builder) {
        builder.stream(this.inputTopic).mapValues(value -> {
            Message message = parseJson((String) value);
           messageService.sendMessage(message);
            return message;
        });
    }

    private Message parseJson(String json) {
        try {

            Gson gson = new Gson();
            Message message = gson.fromJson(json.toString(), new TypeToken<Message>() {}.getType());
            System.out.println(message);
            return message;
        } catch (Exception e) {
            throw new RuntimeException("Error when parsing json", e);
        }
    }
}
