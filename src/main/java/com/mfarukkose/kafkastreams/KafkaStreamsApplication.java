package com.mfarukkose.kafkastreams;

import com.mfarukkose.kafkastreams.streams.MessageStream;
import com.mfarukkose.kafkastreams.streams.MongoWatchStream;

public class KafkaStreamsApplication {

    public static void main(final String[] args) throws Exception {
        MessageStream messageStream = new MessageStream();
        MongoWatchStream mongoWatchStream = new MongoWatchStream();
    }

}
